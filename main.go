package main

import (
	"io"
	"log"
	"net/http"
)

func main() {
	indexHandler := func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "Welcome to Content Gopher!\n")
	}

	http.HandleFunc("/index", indexHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
